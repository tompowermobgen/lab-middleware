const WebSocket = require('ws');
const url = require('url');

let image = '';

class WSServer {
  constructor() {
    this.wss = new WebSocket.Server({
      port: 8888,
    });

    this.clients = [];

    this.wss.on('connection', (ws, req) => {
      const urlData = url.parse(req.url, true);

      this.clients[urlData.pathname] = '';

      ws.on('message', (message) => {
        this.clients[urlData.pathname] = message;
        console.log('message received from', urlData.pathname);
        // console.log('received: %s', message);
      });
    });

    this.getImage = this.getImage.bind(this);
  }

  getImage(path) {
    console.log(this.clients);
    return this.clients[path];
  }
}

module.exports = WSServer;