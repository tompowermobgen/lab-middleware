'use strict';

const request = require('request');
const fs = require('fs');
const haloSDK = require('halo-sdk');
const config = require('../config/halo-config');
const WSServer = require('../ws-server')

class HaloController {
  constructor() {
    this.getModule = this.getModule.bind(this);
    this.wss = new WSServer();
    haloSDK.init(config);
    this.getStream = this.getStream.bind(this);
  }

  getModule(req, res, next) {
    console.log(req.params.id);
    haloSDK.generalContent( 'v1' ).searchModuleInstancesByModuleId( req.params.id ).exec((response, error) => {
      console.log(error);
      if(error) {return res.send('error')}
      console.log(response);
      res.json(response);
      next();
    });
  }

  getStream(req, res, next) {
    // res.send(this.wss.getImage())
    res.contentType('image/jpeg');
    res.end(this.wss.getImage(`/${req.params.id}`), 'binary');
    // haloSDK.generalContent( 'v1' ).searchModuleInstancesByModuleId( '5ae1b43430598c001d244e19' ).exec((response, error) => {
    //   if(error) {return res.send('error')}
    //   response.forEach(item => {
    //     if(item.id === req.params.id) {
    //       console.log(item.values.url);
    //       // res.writeHead(200, {'Content-Type': 'video/mp4'});
    //       res.send(this.wss.getImage())
    //       // ws.pipe(res);
    //     }
    //   });
      
    //   next();
    // });
    next();
  }
}

module.exports = HaloController;