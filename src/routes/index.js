'use strict';

const express = require('express');
const router = express.Router();
const HaloController = require('../controllers/HaloController');

const haloController = new HaloController();

/**
 * Define all routes necessary to manage Valve
 */
router.get('/module/:id', haloController.getModule);
router.get('/stream/:id/:timestamp', haloController.getStream);

module.exports = router;