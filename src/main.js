'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes');
const WSServer = require('./ws-server');
const uuidV4 = require('uuid/v4');

const port = process.env.PORT || 8084;
const app = express();


app.use((req, res, next) => {
  res.header('Access-Control-Allow-Credentials', 'true');

  // const origin = req.headers.origin;

  // res.setHeader('Access-Control-Allow-Origin', origin);
  res.setHeader('Access-Control-Allow-Origin', '*');

  res.header('Access-Control-Allow-Methods', 'GET,POST,DELETE,HEAD,PUT,PATCH,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Accept-Version, Authorization, Channel, X-SSO-MARKET');
  next();
});

app.use((req, res, next) => {
  let id = uuidV4();

  req.headers['X-REQUEST-ID'] = id;
  res.setHeader('X-REQUEST-ID', id);

  next();
});


/**
 * Accept application/json and application/x-www-form-urlencoded body data.
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true,
}));

/**
 * Add routes
 */
app.use('/halo/api', routes);

/**
 * Special middleware to catch sync unhandled errors
 */
// app.use((err, req, res) => {
//   res.status(500).send('Internal server error');
// });

class App {
  constructor() {
    this.server = null;

    this.init = this.init.bind(this);
    this.shutdown = this.shutdown.bind(this);
  }

  /**
   * Initialize server loading the cache system, the database and start socket
   * @returns {Object} Promise
   * @memberOf App
   */
  init() {
    return new Promise((resolve, reject) => {
      try {
        this.server = app.listen(port);
        console.log(`Listening on port ${port}`);
        // const wss = new WSServer();
        return resolve(true);
      }
      catch (e) {
        console.error(e);
        return reject(e);
      }
    })
    
  }

  /**
   * Shutdown the server gracefully with the system dependencies
   * @returns {Object} Promise
   * @memberOf App
   */
  shutdown() {
    return this.server.close();
  }
}

module.exports = new App();