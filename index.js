'use strict';
const App = require('./src/main');

App.init().catch((err) => {
  console.log(err);
});
